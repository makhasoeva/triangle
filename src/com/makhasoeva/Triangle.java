package com.makhasoeva;

import java.util.Arrays;

/**
 * Created by Oli on 17.11.2014.
 */
public class Triangle {
    /**
     * Determines the type of a triangle that can be built with params.
     * Types are right, isosceles, ordinary triangle or non-triangle if cannot build a triangle.
     * @param a the a side of triangle
     * @param b the b side of triangle
     * @param c the c side of triangle
     * @return the type of triangle
     */
    public static TriangleType determineTriangleType(int a, int b, int c) {
        if (!isTriangle(a, b, c)) return TriangleType.NON_TRIANGLE;
        if (a == b || b == c || a == c) return TriangleType.ISOSCELES;
        if (isRight(a, b, c)) return TriangleType.RIGHT;

        return TriangleType.ORDINARY;

    }

    /**
     * Test if a triangle is right.
     * A triangle is right iff the following relation holds true: a*a + b*b = c*c
     * where c represents the length of the hypotenuse
     * and a and b the lengths of the triangle's other two sides.
     * @param a the a side of triangle
     * @param b the b side of triangle
     * @param c the c side of triangle
     * @return true if a triangle is right, false otherwise
     */
    private static boolean isRight(int a, int b, int c) {
        long[] sides = new long[]{a, b, c};
        Arrays.sort(sides);
        return sides[0] * sides[0] + sides[1] * sides[1] == sides[2] * sides[2];
    }

    private static boolean isTriangle(int a, int b, int c) {
        return (a > 0 && b > 0 && c > 0 && (a < (long) b + c) && (b < (long) a + c) && (c < (long) a + b));
    }

    /**
     *  Triangle types are used to indicate a specific type of a triangle
     */
    public enum TriangleType {
        /**
         * RIGHT indicates that a triangle has an angle of 90 degrees
         */
        RIGHT,
        /**
         * ISOSCELES indicates that a triangle has two sides of equal length
         */
        ISOSCELES,
        /**
         * NON_TRIANGLE indicates that triangle can't be build with given sides
         */
        NON_TRIANGLE,
        /**
         * ORDINARY indicates an ordinary triangle
         */
        ORDINARY
    }
}
