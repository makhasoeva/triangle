package com.makhasoeva;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * Created by Oli on 17.11.2014.
 */
@RunWith(Parameterized.class)
public class TriangleTest {
    private int a;
    private int b;
    private int c;

    @Parameterized.Parameters
    public static Collection intTriples() {
        ArrayList<Integer[]> testCases = new ArrayList<>();
        testCases.add(new Integer[]{-2, 8, 11});
        testCases.add(new Integer[]{20, -1, 5});
        testCases.add(new Integer[]{1, 7, 0});
        return testCases;
    }

    public TriangleTest(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Test
    public void testNonPositiveArguments() {
            String message = "The method should return a non-triangle value for non-positive arguments. " +
                    "Test case: [" + a + "], [" + b + "], [" + c + "]";
            assertEquals(message, Triangle.TriangleType.NON_TRIANGLE, Triangle.determineTriangleType(a, b, c));
    }

    @Test
    public void testIllegalSidesSizes() {
        ArrayList<Integer[]> testCases = new ArrayList<>();
        testCases.add(new Integer[]{2, 8, 11});
        testCases.add(new Integer[]{20, 1, 5});
        testCases.add(new Integer[]{1, 7, 2});

        for (Integer[] testCase : testCases) {
            String message = "The method should return a non-triangle value for" +
                    " arguments that don't satisfy the triangle inequality. " +
                    "Test case: [" + testCase[0] + "], [" + testCase[1] + "], [" + testCase[2] + "]";
            assertEquals(message, Triangle.TriangleType.NON_TRIANGLE, Triangle.determineTriangleType(testCase[0], testCase[1], testCase[2]));
        }

    }

    @Test
    public void testRightTriangles() {
        ArrayList<Integer[]> testCases = new ArrayList<>();
        testCases.add(new Integer[]{3, 4, 5});
        testCases.add(new Integer[]{12, 16, 20});
        testCases.add(new Integer[]{15, 36, 39});

        for (Integer[] testCase : testCases) {
            String message = "The method should return a right value for a pythagorean triple. " +
                    "Test case: [" + testCase[0] + "], [" + testCase[1] + "], [" + testCase[2] + "]";
            assertEquals(message, Triangle.TriangleType.RIGHT, Triangle.determineTriangleType(testCase[0], testCase[1], testCase[2]));
        }
    }

    @Test
    public void testIsoscelesTriangles() {
        ArrayList<Integer[]> testCases = new ArrayList<>();
        testCases.add(new Integer[]{4, 4, 5});
        testCases.add(new Integer[]{12, 20, 12});
        testCases.add(new Integer[]{15, 39, 39});

        for (Integer[] testCase : testCases) {
            String message = "The method should return an isosceles value. " +
                    "Test case: [" + testCase[0] + "], [" + testCase[1] + "], [" + testCase[2] + "]";
            assertEquals(message, Triangle.TriangleType.ISOSCELES, Triangle.determineTriangleType(testCase[0], testCase[1], testCase[2]));
        }
    }

    @Test
    public void testOrdinaryTriangles() {
        ArrayList<Integer[]> testCases = new ArrayList<>();
        testCases.add(new Integer[]{2, 4, 5});
        testCases.add(new Integer[]{12, 45, 35});
        testCases.add(new Integer[]{8, 9, 10});

        for (Integer[] testCase : testCases) {
            String message = "The method should return an ordinary value. Test case:["
                    + testCase[0] + "],[" + testCase[1] + "],[" + testCase[2] + "]";
            assertEquals(message, Triangle.TriangleType.ORDINARY, Triangle.determineTriangleType(testCase[0], testCase[1], testCase[2]));
        }
    }

    @Test
    public void testBigIntegers() {
        int intMax = Integer.MAX_VALUE;

        assertEquals("The method should return an isosceles value (Big integers case). " +
                        "Test case: [Integer.MAX_VALUE],[Integer.MAX_VALUE],[Integer.MAX_VALUE] ",
                Triangle.TriangleType.ISOSCELES, Triangle.determineTriangleType(intMax, intMax, intMax));
        assertEquals("The method should return an ordinary value (Big integers case). " +
                "Test case: [Integer.MAX_VALUE / 2 + 1],[Integer.MAX_VALUE / 2],[Integer.MAX_VALUE / 3]",
                Triangle.TriangleType.ORDINARY, Triangle.determineTriangleType(intMax / 2 + 1, intMax / 2, intMax / 3));
    }


}
